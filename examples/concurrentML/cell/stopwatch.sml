structure StopWatch = struct
open CML
type clock = { hour : int, minute : int, secs : int }
val lineKill   = "\b\b\b\b\b\b\b\b\b"

fun program () =
    let val cCell : clock Cell.cell = Cell.make {hour = 0 , minute = 0, secs = 0}
	fun update () =
	    let val tm = Cell.get cCell
		val h = #hour tm
		val m = #minute tm
		val s = #secs tm
		val sp = (s+1)
		val mp = if sp = 60 then (m + 1)
			 else m
		val hp = if mp = 60 then h + 1
			 else h
	    in

		Cell.put cCell {hour = hp, minute = mp mod 60, secs = sp mod 60};
		sync (timeOutEvt (Time.fromSeconds 1)) ;
		update ()
	    end
	fun show () : unit =
	    let val tm = Cell.get cCell
		val h = #hour tm
		val m = #minute tm
		val s = #secs tm
	    in TextIO.print ( lineKill ^
		              Int.toString h ^ ":" ^
			      Int.toString m ^ ":" ^
			      Int.toString s
			    );
	       sync (timeOutEvt (Time.fromReal 0.5));
	       show ()
	    end	
    in spawn update;
       show ()
    end
fun main (name, args) = RunCML.doit (program, NONE)
end

val _ = StopWatch.main ("foo", [])
