# Concurrent ML

This directory contains examples of use of Concurrent ML. Since there
is a dependency on concurrent ml library, you will need to use the
smlnj compilation manager (CM).

The following lets you run the pong example from command line.

```
ml-build sources.cm  Pong.main # use CM for building the heap object
sml @SMLload pong.amd64-linux # Load the image and execute.
```
The name `pong.amd64-linux` comes from the fact that I am running it on 64-bit
Intel architecture under linux. It might be different in your case. Search the directory
for the exact name of the image.


Alternatively, you can run the code inside sml interpreter by using
the compilation manager from the top level.

```
CM.make "sources.cm";
Pong.main ("pong", []);
```


## The `timeOutEvt` bug

I seem to be having problem with the `timeOutEvt` function in
`smlnj`. So I will be using the mlton compiler exclusively for the
examples in this directory. You need to compile the appropriate
`*.mlb` file with mlton. For example the following commands will run
the stop watch example.

```
cd cell
mlton stopwatch.mlb
./stopwatch
``
