# Starting with Prolog

1. On your private machine install one of either gnuprolog or swiprolog.
   The latter is more popular for some definition of popular.

2. With basic predicates man(X), women(X), and parent(X,Y) define
   relations like father, mother, grand-father, grand-mother, brother
   and sister.

3. Test it out with some basic database created.
