# Lists and Trees

Deadline: 22 September 2023, 23:59

In this assignment we look at functions on binary trees.

1. Define the data type `'a tree` that captures a binary tree.

2. Write a function `mapT` analogues to `map` for list ?  First write
   its type and then complete its definition.

3. Define the in-order, pre-order and post-order traversal of the
   binary tree returning the list of nodes in the given order. First
   write down the type of the function(s) and then go about defining
   them.

4. Define a tree `fold` function analogous to the `foldr` and `foldl`
   functions.

5. Express the above traversals in terms of tree fold.

6. Define the rotate clockwise function for binary trees. Pictorially
   this rotation function is defined as the following.



                   a                   b
                  / \                 / \
                 / ⭮ \               /   \
                b    🌲₃  ======>   🌲₁   a
               / \                       / \
              /   \                     /   \
             🌲₁   🌲₂                 🌲₂    🌲₃

	If the left subtree of the root is null then rotation is identity operation.
